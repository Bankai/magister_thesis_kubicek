# 31 Oct 2023

Some introductory papers:
- MRP 2020 (CoNLL 2020 Shared Task: Cross-Framework Meaning Representation Parsing)
  - https://aclanthology.org/2020.conll-shared.1/: overview paper
  - https://aclanthology.org/2020.conll-shared.5/: our entry
  - https://aclanthology.org/2020.conll-shared.4/: other co-winner

- Maybe some of the AMR parsing papers, for example at:
  - AMR 2.0: https://paperswithcode.com/sota/amr-parsing-on-ldc2017t10
  - AMR 3.0: https://paperswithcode.com/sota/amr-parsing-on-ldc2020t02

  However, there is a lot of papers there, so just some of them.

- Even if the goal is a general sentence-to-graph model, there are a few
  possible NLP tasks to evaluate on:
  - Multilingual UMR https://umr4nlp.github.io/web/ is the main target;
    the description of the representation is at
    https://doi.org/10.1007/s13218-021-00722-w (it is enough to skim it;
    the PDF can be downloaded if the URL is accessed using university IPs).
    - This implies AMR, because it can be used to
      - pre-train a model, which could be fine-tuned with less UMR data
      - benchmark how good our model is

  - Tectogrammatical layer of PDT-C 1.0 https://ufal.mff.cuni.cz/pdt-c, which
    is available also in the MRP format.

  - Maybe multilingual coreference as a simpler task
    - 2022 shared task is at https://ufal.mff.cuni.cz/corefud/crac22
    - 2023 shared task is at https://ufal.mff.cuni.cz/corefud/crac23, but the papers
      will be published on Dec 7

  - Anything else?

# 20 Nov 2023
## thoughts and summaries for articles
- [UFAL at MRP 2020: Permutation-invariant Semantic Parsing in PERIN](https://aclanthology.org/2020.conll-shared.5.pdf)
  Important Ideas:
    - relative label encodings
    - permutation invariant loss
    - general structure of predicting the whole graph at one instead of seq2seq
    - contextual embedding extraction
- [Structural Adapters in Pretrained Language Models for AMR-to-Text Generation](https://arxiv.org/pdf/2103.09120.pdf)

  This paper is about graph to text finetuning. It involves using pre-trained language model and finetuning it in a specific way.
  As this is graph-to-text, the text is firstly linearized into some form of text with additional tags(or words) arising from the AMR.
  The task is to then transform this represenation to a sensible language.

  This finetuning is done by including a trainable graph convolution after every transformer layer. They also experiment with relational graph convolutions which allow for different treatment of outgoing and incoming edges which seems to improve the performance. 

  Different ways to connect tokens accroding to a graph are evaluated. Also finetuning only encoder or only decoder is also considered.

  Overall this seems to be a better performing method of finetuning LLM when a graph representation is avaiable  with less parameters.

- [Incorporating Graph Information in Transformer-based AMR Parsing](https://arxiv.org/pdf/2306.13467v1.pdf)

  This paper build up on the previous paper abou Structural adapters. 
  
  First it presents a way how you would do this with knowledge distilation
  It tries to use the provided output graph information in 
  the encoder for the training data using struct adapt to train a seq2seq architecture to output the provided graph.

  It then trains a new encoder (with the same decoder frozen from previous step), that uses knowledge distilation from the previous step.

  Then they also propose a model which does two forward passes one with and one without the structural adapater and using kl-loss
  tries to make the one without do the same thing.

  They make also some changes without claim as to why they did them namely they remove layernorm and changed nonlinearity function.
  One more thing discused was handling of things in graph that have no representation in the text. This seems to be an important thing as it improved the teacher model significantly.

  According to their data their setup of self distilation shows improvements over simple knowledge distilation. 

- [Graph Pre-training for AMR Parsing and Generation](https://arxiv.org/pdf/2203.07836v4.pdf)
  
  This paper experiments with essentialy new strategies of training a transformer, meaning, they add tasks and different ways of introducing noise.

  They basicaly linearize the graphs, define a way how it can be masked (either some part of the graph in its entirety is dissapears and is replaced by one token or the tokens representing the underlying information are masked but are still present atleast in count and shape.)

  Then they defined tasks which get both a graph and text, in which atleast one is atleast masked or even missing,
  and are then tasked to produce either the graph or the text.

  
- [BiBL: AMR Parsing and Generation with Bidirectional Bayesian Learning](https://aclanthology.org/2022.coling-1.485.pdf)

  To me this seems a bit similar in spirit to the previous paper, it adds a purely generational task.

- [ENSEMBLING GRAPH PREDICTIONS FOR AMR PARSING](https://arxiv.org/pdf/2110.09131v2.pdf)

  Basicaly when you have two labeled graphs (with labels on edges and nodes), you can define an injective mapping from vertices
  of on graph to the other.

  When you have such a matching you can compute where the the two graphs have the same labels and thus define a supporting labels.
  One task is then to find such a matching of maximum support between two graphs.

  When we have multiple graphs, and one particular graph and matchings between the particular and the rest we can compute the total support as the sums of support.

  When we then get m graphs to ensemble, for each of those we first find best matching to the rest, and then count votes where we match and where we disagree. 

  Then a trimming is done, this is either done so that only the best wins or only the ones above certain vote count wins.
  
  
  From this we have several candidates and we pick one which has the best total support.

  The best match is computet via the hill climbing method from [Smatch: an Evaluation Metric for Semantic Feature Structures](https://aclanthology.org/P13-2131.pdf)

  I also tried looking at [Maximum Bayes Smatch Ensemble Distillation for AMR Parsing](https://arxiv.org/pdf/2112.07790v2.pdf)
  which seems to build of of this ensembling along with some silver data generation and distilation but I couldn't make heads or tails of it.

## ideas and thoughts
 - Adversarial network have a text to graph, and then basicaly a struct adapter which has to decide whether it is a gold created graph. Preferably with some masking on input
 - it seems to me like generating correct anchored labelless graph structure is really important and could be used as pretraining.
 It would allow us to very easily use different "graph corpi", because even though they are different they still try to find in some ways the connections between things. These anchored labelless  graphs could then serve as basis for structural adapters for finetuning the encoder of the real parser.
 - We were previously talking about how to leverage the different labeled graph parsing. One of our ideas was to make it a multigraph parser capable of doing multiple kinds of graphs.
 - Because of lack of data for UMR-parsing, it might be a good idea to consider silver data generation.
 - When doing other ML task regularization is the key, when reading the articles I noticed that apart from pretraining with masked data noone seems to be using masking and things like that as a form of regularization. 
 - From a brief look at the data present in the umr, there is also a lot of work to be done for working with external source of truth like wikipedia. Relative encoding rules do not seem like a good fit for handling these cases but i am not sure whether embedding wikipedia into vector database is a viable solution.
 - How successful are rules for encoding label for chinese etc.
 - RN there seems to be atleast some things which seem like a mistake to me "corpi\umr-v1.0\english\english_umr-0005.txt" line 56 "FULLfull-affirmative". I think a good thing maybe not for us, but for them would be to provide some automatic schema check or something. An XML structure and schema would be a god send.

# 2 Feb 2024
## thoughts and summaries for articles
- [Improving Pre-trained Language Model Fine-tuning with Noise Stability Regularization](https://arxiv.org/pdf/2206.05658.pdf)
