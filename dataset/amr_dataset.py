from __future__ import annotations
from itertools import chain
from pathlib import Path
from queue import Queue
from typing import Any, Iterator, NamedTuple, Self, Optional, Tuple
import numpy as np
import torch.utils.data
from tokenizers import AddedToken
from dataclasses import dataclass, field


from dataset.parser.amr_parser import SimpleRepresentation
from torch.utils.data import Dataset
import json
from transformers import T5Tokenizer, MT5Tokenizer
import torch
import penman
from torch.utils.data import Sampler


class RawAMRDataset(Dataset):
    def __init__(
        self,
        path: str | Path,
        cache_dir: str | Path = ".cache/raw_amr",
        name: Optional[str] = None,
    ) -> None:
        super().__init__()
        path = Path(path)

        cache_dir = Path(cache_dir)
        cache_dir.mkdir(exist_ok=True, parents=True)

        if name is None:
            name = path.with_suffix(".json").name
        else:
            name = Path(name).with_suffix(".json").name

        cache = cache_dir / name
        if cache.exists():
            with cache.open("r", encoding="utf-8") as f:
                self.data: list[str] = json.load(f)
            return

        # if cache_dir/name
        files: list[Path] = []

        if path.is_dir():
            files = list(path.glob("*.txt"))
        elif path.is_file():
            files = [path]
        file_parser = SimpleRepresentation()

        graphs = chain(*map(file_parser.get_graph_str_representation, files))

        self.data = list(graphs)
        with cache.open("w", encoding="utf-8") as f:
            json.dump(self.data, f, indent="\t")

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index) -> str:
        return self.data[index]


class AMRWithSimpleTarget(NamedTuple):
    sentence: str
    target: str
    penman: str


class AMRWithSimpleTargetsDataset(Dataset):
    def __init__(
        self,
        path: str | Path,
        cache_dir: str | Path = ".cache/simple_amr",
        name: Optional[str] = None,
    ) -> None:
        super().__init__()
        path = Path(path)

        cache_dir = Path(cache_dir)
        cache_dir.mkdir(exist_ok=True, parents=True)

        self.data: list[AMRWithSimpleTarget] = []
        if name is None:
            name = path.with_suffix(".json").name
        else:
            name = Path(name).with_suffix(".json").name

        cache = cache_dir / name
        if cache.exists():
            with cache.open("r", encoding="utf-8") as f:
                json_data = json.load(f)
                self.special_tokens: list[str] = json_data["special_tokens"]
                for d in json_data["data"]:
                    self.data.append(AMRWithSimpleTarget(*d))  # type: ignore

            return

        special_tokens = set()
        raw_amr = RawAMRDataset(path)
        for i in range(len(raw_amr)):
            (sentence, target, _special_tokens) = SimpleRepresentation.to(raw_amr[i])

            pen = "\n".join(
                filter(lambda x: not x.startswith("# ::"), raw_amr[i].splitlines())
            )
            pen = penman.encode(penman.decode(pen), compact=True)
            self.data.append(AMRWithSimpleTarget(sentence, target, pen))
            special_tokens = special_tokens.union(_special_tokens)

        self.special_tokens: list[str] = sorted(list(special_tokens) + ["<node>"])
        with cache.open("w", encoding="utf-8") as f:
            json.dump(
                {"special_tokens": self.special_tokens, "data": self.data},
                f,
                indent="\t",
            )

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index) -> AMRWithSimpleTarget:
        return self.data[index]

    @classmethod
    def get_unsplit_dataset(
        cls,
        amr_root: Path = Path("corpi/LDC2017T10_amr_2.0/data/amrs/unsplit/"),
    ) -> Self:

        return cls(amr_root)


class AMRWithSimpleTargetsTokenizedDataset(Dataset):
    def __init__(
        self,
        path: str | Path,
        model: Optional[str] = None,
        tokenizer: Optional[T5Tokenizer] = None,
    ) -> None:
        super().__init__()
        path = Path(path)

        simple_amr = AMRWithSimpleTargetsDataset(path)
        assert (model is None) ^ (tokenizer is None)
        if tokenizer is None:
            assert isinstance(model, str)
            if "mt5" in model:
                _tokenizer = MT5Tokenizer
            else:
                _tokenizer = T5Tokenizer
            assert not _tokenizer is None
            assert not _tokenizer.from_pretrained is None
            self.tokenizer: T5Tokenizer = _tokenizer.from_pretrained(
                model, cache_dir=".cache/hugging_face"
            )
            tokens = [
                AddedToken(t, single_word=True) for t in simple_amr.special_tokens
            ]
            self.tokenizer.add_tokens(tokens)  # type: ignore
        else:
            self.tokenizer = tokenizer

        features, target, penmans = (
            [i.sentence for i in simple_amr.data],
            [i.target for i in simple_amr.data],
            [i.penman for i in simple_amr.data],
        )

        self.data = self.tokenizer(
            text=features, text_target=target, return_tensors="np"
        )
        self.data["penman"] = penmans
        # print(str(path))
        # print(np.max(np.fromiter(map(len, self.data["input_ids"]), dtype=np.int64)))
        # print(np.max(np.fromiter(map(len, self.data["labels"]), dtype=np.int64)))
        # # indices = np.argsort(
        # #     np.fromiter(map(len, self.data["input_ids"]), dtype=np.int64)  # type: ignore
        # #     + np.fromiter(map(len, self.data["labels"]), dtype=np.int64)  # type: ignore
        # # )

        # def reorder(l, idxs):
        #     return np.array(l, dtype=object)[idxs].tolist()

        # for k in self.data:
        #     self.data[k] = reorder(self.data[k], indices)

        self.len = len(features)

    def __len__(self):
        return self.len

    def __getitem__(self, index):
        input_ids = torch.from_numpy(self.data["input_ids"][index])  # type: ignore
        labels = torch.from_numpy(self.data["labels"][index])  # type: ignore
        penman = self.data["penman"][index]  # type: ignore

        return {"input_ids": input_ids, "labels": labels, "penman": penman}

    @classmethod
    def get_unsplit_dataset(
        cls,
        model: str = "t5-base",
        amr_root: Path = Path("corpi/LDC2017T10_amr_2.0/data/amrs/unsplit/"),
    ) -> Self:

        return cls(amr_root, model=model)

    @classmethod
    def get_split_dataset(
        cls,
        model: str = "t5-base",
    ) -> DatasetSplit:
        tokenizer = cls.get_unsplit_dataset(model=model).tokenizer
        train = Path("corpi/LDC2017T10_amr_2.0/data/amrs/split/training/")
        dev = Path("corpi/LDC2017T10_amr_2.0/data/amrs/split/dev/")
        test = Path("corpi/LDC2017T10_amr_2.0/data/amrs/split/test/")
        return DatasetSplit(
            cls(train, tokenizer=tokenizer),
            cls(dev, tokenizer=tokenizer),
            cls(test, tokenizer=tokenizer),
        )


class DatasetSplit(NamedTuple):
    train: AMRWithSimpleTargetsTokenizedDataset
    dev: AMRWithSimpleTargetsTokenizedDataset
    test: AMRWithSimpleTargetsTokenizedDataset


@dataclass
class Bucket:
    start: Optional[int] = None
    end: Optional[int] = None
    max_work: int = 0
    max_batch_size: Optional[int] = None
    data: list[int] = field(default_factory=list, init=False)
    current_max: int = field(init=False)
    batch_queue: Optional[Queue[list[int]]] = None
    members: int = 0
    active: bool = True

    def __post_init__(self):
        if self.start and self.end:
            assert self.start <= self.end
        assert self.max_work > 0
        assert not self.batch_queue is None
        for i in [self.start, self.end]:
            if i:
                assert i <= self.max_work

        self.current_max = 0

        assert self.max_batch_size is None or self.max_batch_size > 0
        assert self.members > 0

    def __contains__(self, item: int) -> bool:
        if self.start and item < self.start:
            return False
        if self.end and item > self.end:
            return False
        return True

    def _post(self):
        assert self.batch_queue
        if self.data:
            self.batch_queue.put(self.data)
            self.data = []

    def add(self, size, index) -> bool:
        if not size in self:
            return False
        if not self.active:
            return True
        new_max = max(self.current_max, size)
        if (len(self.data) + 1) * new_max >= self.max_work:
            self._post()
            new_max = size
        elif len(self.data) == self.max_batch_size:
            self._post()
            new_max = size
        self.data.append(index)
        self.current_max = new_max
        return True

    def cleanup(self):
        self._post()


class DynamicBatchSampler(Sampler[list[int]]):
    batch_queue: Queue[list[int]] = Queue()

    def _construct_buckets(
        self, sorted_list: list[int], target_efficiency, max_work, max_batch_size
    ) -> list[Bucket]:
        if len(sorted_list) == 0:
            return []

        def compute_efficiency(l: list[int]):
            worst_batch = [l[-1], l[0]]
            for i in l[1:-1]:
                if (len(worst_batch) + 1) * worst_batch[0] <= max_work:
                    worst_batch.append(i)
                else:
                    break

            if max_batch_size:
                worst_batch = worst_batch[:max_batch_size]
            eff = sum(worst_batch) / (max(worst_batch) * len(worst_batch))
            return eff

        eff = compute_efficiency(sorted_list)

        if eff >= target_efficiency:
            return [
                Bucket(
                    sorted_list[0],
                    sorted_list[-1],
                    max_work,
                    max_batch_size,
                    self.batch_queue,
                    len(sorted_list),
                )
            ]
        else:
            groups: list[list[int]] = []
            current: list[int] = []
            for i in sorted_list:
                if len(current) == 0 or current[-1] == i:
                    current.append(i)
                else:
                    groups.append(current)
                    current = []
            else:
                if current:
                    groups.append(current)

            left_idx = 0
            left = []

            right_idx = len(groups) - 1
            right = []
            while True:
                if len(left) <= len(right):
                    left.extend(groups[left_idx])
                    left_idx += 1
                else:
                    right.extend(groups[right_idx])
                    right_idx -= 1
                if left_idx > right_idx:
                    break
            right.reverse()

            return self._construct_buckets(
                left,
                target_efficiency,
                max_work,
                max_batch_size,
            ) + self._construct_buckets(
                right,
                target_efficiency,
                max_work,
                max_batch_size,
            )

    def construct_buckets(
        self, sorted_list: list[int], target_efficiency, max_work, max_batch_size
    ) -> list[Bucket]:
        res = self._construct_buckets(
            sorted_list, target_efficiency, max_work, max_batch_size
        )

        res[0].start = None
        res[-1].end = None
        for i in range(len(res) - 1):
            next_start = res[i + 1].start
            assert not next_start is None

            res[i].end = next_start - 1
        return res

    def __init__(
        self,
        dataset: AMRWithSimpleTargetsTokenizedDataset,
        max_work: Optional[int] = None,
        work_efficiency: float = 0.85,
        max_batch_size: Optional[int] = None,
        deactivate_last: int = 0,
    ) -> None:
        self.dataset = dataset
        srt_targets: list[int] = sorted(np.fromiter(map(len, self.dataset.data["labels"]), dtype=np.int64))  # type: ignore

        self.max_work = max_work if max_work else srt_targets[-1]
        self.buckets: list[Bucket] = self.construct_buckets(
            srt_targets, work_efficiency, self.max_work, max_batch_size
        )

        if deactivate_last > 0:
            for b in list(reversed(self.buckets))[:deactivate_last]:
                b.active = False
                print(f"Deactivated {b=}")

            tmp = [b.end for b in self.buckets if b.active and not b.end is None]

            self.max_work = (max(tmp) + self.max_work) // 2
            for b in self.buckets:
                b.max_work = self.max_work
            invalidated_members = 0
            for b in self.buckets:
                if not b.active:
                    invalidated_members += b.members
            if invalidated_members > 0:
                print(f"INFO: {invalidated_members=}")
        # if max_work:
        #     self.max_work = max_work
        #     for b in self.buckets:
        #         b.max_work = self.max_work
        self.precomputed: Optional[list[list[int]]] = None
        # for b in self.buckets:
        #     print(b)

    def __len__(self):
        if not self.precomputed:
            self.make_iteration()
        assert self.precomputed
        return len(self.precomputed)

    def make_iteration(self):
        self.precomputed = list(iter(self))

    def __iter__(self) -> Iterator[list[int]]:
        if self.precomputed:
            tmp = self.precomputed
            self.precomputed = None
            yield from tmp
            return
        n = len(self.dataset)
        seed = int(torch.empty((), dtype=torch.int64).random_().item())
        generator = torch.Generator()
        generator.manual_seed(seed)
        permutation = torch.randperm(n, generator=generator).tolist()
        for i in permutation:
            for b in self.buckets:
                if b.add(len(self.dataset[i]["labels"]), i):
                    break
            else:
                print("weird")
            if self.batch_queue.qsize() > 0:
                yield self.batch_queue.get()
        for b in self.buckets:
            b.cleanup()
        while self.batch_queue.qsize() > 0:
            yield self.batch_queue.get()


if __name__ == "__main__":
    pass
    import sys

    sys.path.append("..")
    dataset = AMRWithSimpleTargetsTokenizedDataset.get_split_dataset()
    for i in DynamicBatchSampler(
        dataset.train,
    ):
        pass
