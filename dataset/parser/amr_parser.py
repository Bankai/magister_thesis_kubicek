from typing import Iterable, Tuple, Generator, NamedTuple
from pathlib import Path
from typing import Iterator

from abc import abstractmethod, ABC


import penman
import penman.graph
import penman.constant
from unidecode import unidecode
import smatch
from tqdm import tqdm


def my_normalize(item: str):
    item = item.lower()

    def normalize(i):
        i = i.strip()
        i = i.strip("\"'")
        i = i.rstrip("_")
        return i

    while item != normalize(item):
        item = normalize(item)

    return item


smatch.normalize = my_normalize


class AMRParser(ABC):
    # From https://stackoverflow.com/questions/6609895/efficiently-replace-bad-characters
    _convert_dict = {
        b"\xc2\x82": b",",  # High code comma
        b"\xc2\x84": b",,",  # High code double comma
        b"\xc2\x85": b"...",  # Tripple dot
        b"\xc2\x88": b"^",  # High carat
        b"\xc2\x91": b"'",  # Forward single quote - '\x27'
        b"\xc2\x92": b"'",  # Reverse single quote - '\x27'
        b"\xc2\x93": b'"',  # Forward double quote - '\x22'
        b"\xc2\x94": b'"',  # Reverse double quote - '\x22'
        b"\xc2\x95": b" ",
        b"\xc2\x96": b"-",  # High hyphen
        b"\xc2\x97": b"--",  # Double hyphen
        b"\xc2\x99": b" ",
        b"\xc2\xa0": b" ",
        b"\xc2\xa6": b"|",  # Split vertical bar
        b"\xc2\xab": b"<<",  # Double less than
        b"\xc2\xbb": b">>",  # Double greater than
        b"\xc2\xbc": b"1/4",  # one quarter
        b"\xc2\xbd": b"1/2",  # one half
        b"\xc2\xbe": b"3/4",  # three quarters
        b"\xca\xbf": b"'",  # c-single quote - '\x27'
        b"\xcc\xa8": b"",  # modifier - under curve
        b"\xcc\xb1": b"",  # modifier - under line
    }

    @classmethod
    @abstractmethod
    def to(cls, graph_raw: str):
        raise NotImplementedError()

    @classmethod
    @abstractmethod
    def from_str(cls, simple: str, special_tokens) -> str:
        raise NotImplementedError()

    @classmethod
    def _compute_smatch(
        cls, a: str, b: str, special_tokens: set[str], convert_a: bool, convert_b: bool
    ) -> tuple[float, float, float]:
        if convert_a:
            a = cls.from_str(a, special_tokens)
        if convert_b:
            b = cls.from_str(b, special_tokens)
        try:
            smatch.match_triple_dict.clear()
            res = smatch.get_amr_match(a, b)
            return res
        except Exception as e:
            print(e)
            print(f"{a=}")
            print(f"{b=}")
            return 0.0, 0.0, 0.0

    @classmethod
    def compute_smatch(
        cls,
        a_list: list[str],
        b_list: list[str],
        special_tokens: set[str],
        convert_a: bool = True,
        convert_b: bool = True,
        iteration_number: int = 5,
    ) -> float:
        res = []
        assert len(a_list) == len(b_list)
        total = len(a_list)

        def compute_score():
            return smatch.compute_f(*map(sum, zip(*res)))[2]

        smatch.iteration_num = iteration_number
        for i, (a, b) in (
            progress_bar := tqdm(
                enumerate(zip(iter(a_list), iter(b_list))), total=total
            )
        ):
            try:
                triple = cls._compute_smatch(a, b, special_tokens, convert_a, convert_b)
            except AssertionError as e:
                print(f"Could not compute smatch for {i}-th.")
                print(f"{a=}")
                print(f"{b=}")
                print(e)
                continue

            res.append(triple)
            progress_bar.set_description(f"Score: {compute_score()}")

        return compute_score()

    @classmethod
    def _to_ascii(cls, raw_bytes: bytes):
        for code, repl in cls._convert_dict.items():
            raw_bytes = raw_bytes.replace(code, repl)
        text = raw_bytes.decode("utf-8")
        text = unidecode(text)
        return text

    def get_graph_str_representation(self, input: str | Path) -> Iterator[str]:
        with open(input, "rb") as file:
            file_text: bytes = file.read()

        data = self._to_ascii(file_text)
        # Strip off non-amr header info (see start of Little Prince corpus)
        lines = [
            l
            for l in data.splitlines()
            if not (l.startswith("#") and not l.startswith("# ::"))
        ]
        data = "\n".join(lines)
        # Split into different entries based on a space (ie.. extra linefeed) between the graph and text
        entries = data.split("\n\n")  # split via standard amr
        entries = [e.strip() for e in entries]  # clean-up line-feeds, spaces, etc
        entries = [e for e in entries if e]  # remove any empty entries

        yield from entries

    @classmethod
    def dfs(cls, g: penman.graph.Graph) -> Generator[penman.graph.Variable, None, None]:
        assert g.top
        stack: list[penman.graph.Variable | penman.graph.Edge] = []

        stack.append(g.top)

        visited: set[penman.graph.Variable] = set()
        while stack:
            node_or_edge = stack.pop()
            if isinstance(node_or_edge, penman.graph.Variable):
                node = node_or_edge
            else:
                node = node_or_edge.target
            if node in visited:
                continue
            visited.add(node)
            yield node
            neighbors = []
            neighbors.extend(g.edges(node))
            neighbors.extend((e.source for e in g.edges(target=node)))
            # We probably want to take care of the first think in order, this requieres us to put it at the end of the stack
            neighbors.reverse()
            stack.extend(neighbors)

        # return reentrant_edges


class SimpleRepresentation(AMRParser):
    @classmethod
    def to(cls, graph_raw: str) -> Tuple[str, str, set[str]]:
        try:
            graph = penman.decode(graph_raw)
        except Exception as e:
            print(str(e))
            print(graph_raw)
            exit()

        variable_sequence = list(cls.dfs(graph))
        instances = graph.instances()

        class Edge(NamedTuple):
            edge: penman.graph.Edge
            relative_offset: int

        graph_sequence: list[Edge | penman.graph.Instance | penman.graph.Attribute] = []

        for index, v in enumerate(variable_sequence):
            instance = [i for i in instances if i.source == v]
            assert len(instance) == 1
            instance = instance[0]

            graph_sequence.append(instance)

            attrs = sorted(graph.attributes(source=v), key=lambda x: x.role)
            graph_sequence.extend(attrs)

            edges: list[Edge] = sorted(
                [
                    Edge(e, variable_sequence.index(e.source) - index)
                    for e in graph.edges(target=v)
                ],
                key=lambda x: (x.relative_offset, x.edge.role),
            )
            graph_sequence.extend(edges)

        special_tokens: set[str] = set()

        def make_edge_token(s: str) -> str:
            tkn = f"<edge{s}>"
            special_tokens.add(tkn)
            return tkn

        def make_attribute_token(s: str) -> str:
            tkn = f"<attr{s}>"
            special_tokens.add(tkn)
            return tkn

        target = []
        for element in graph_sequence:
            match element:
                case Edge():
                    ext = [
                        make_edge_token(element.edge.role),
                        str(element.relative_offset),
                    ]
                case penman.graph.Attribute():

                    ext = [
                        make_attribute_token(element.role),
                        str(penman.constant.evaluate(str(element.target))),
                    ]
                case penman.graph.Instance():
                    ext = ["<node>", str(element.target)]
                case _:
                    ext = []
            for e in ext:
                assert len(e.strip()) > 0
            target.extend(ext)
        target = " ".join(target)

        if "snt" in graph.metadata:
            sentence: str = graph.metadata["snt"]
        else:
            sentence = ""

        return sentence, target, special_tokens

    @classmethod
    def from_str(cls, simple: str, special_tokens) -> str:
        instances: list[penman.graph.Instance] = []
        attrs: list[penman.graph.Attribute] = []
        edges: list[penman.graph.Edge] = []
        words = simple.split()

        def is_number(num: str) -> bool:
            try:
                int(num)
                return True
            except:
                return False

        graph_starts = [i for i, w in enumerate(words) if w in special_tokens]

        def node_name(num) -> str:
            return f"node_{num}"

        for i in range(len(graph_starts)):
            start_token = words[graph_starts[i]]
            if i + 1 < len(graph_starts):
                rest = " ".join(words[graph_starts[i] + 1 : graph_starts[i + 1]])
            else:
                rest = " ".join(words[graph_starts[i] + 1 :])

            start_token = start_token[1:-1]
            if start_token == "node":
                rest = penman.constant.quote(rest.strip("'\""))
                instance = penman.graph.Instance(
                    node_name(len(instances)), ":instance", rest
                )
                instances.append(instance)
                continue
            role = start_token[start_token.find(":") :]
            token_type = start_token[: start_token.find(":")]

            if token_type == "edge" and is_number(rest):
                if len(instances) == 0:
                    target = 0
                else:
                    target = len(instances) - 1
                source = target + int(rest)

                edge = penman.graph.Edge(node_name(source), role, node_name(target))
                edges.append(edge)
            elif token_type == "attr":
                if len(instances) == 0:
                    source = 0
                else:
                    source = len(instances) - 1
                source = node_name(source)

                rest = penman.constant.quote(rest.strip("'\""))
                attr = penman.graph.Attribute(source, role, rest)
                attrs.append(attr)
            else:
                print("[Mistake when transforming to penmam notation]")
                print(f"[{start_token=} {rest=}]")

        def is_good(e: penman.graph.Edge | penman.graph.Attribute) -> bool:
            nodes = [i.source for i in instances]
            if not e.source in nodes:
                return False
            if isinstance(e, penman.graph.Attribute):
                return True
            if not e.target in nodes:
                return False
            return True

        edges = [e for e in edges if is_good(e)]
        attrs = [e for e in attrs if is_good(e)]

        g = penman.Graph(instances + attrs + edges)
        reachable = set(cls.dfs(g))
        instances = [i for i in instances if i.source in reachable]

        edges = [e for e in edges if is_good(e)]
        attrs = [e for e in attrs if is_good(e)]

        g = penman.Graph(instances + attrs + edges)
        penman_amr = penman.encode(g, compact=True)
        return penman_amr


# def test_dataset_for_conversion():
#     import sys

#     sys.path.append("dataset")
#     sys.path.append(".")

#     from amr_dataset import AMRWithSimpleTargetsDataset
#     from tqdm import tqdm

#     smatch.iteration_num = 2

#     unsplit_ds = AMRWithSimpleTargetsDataset.get_unsplit_dataset()
#     tokens = set(unsplit_ds.special_tokens)
#     l = [0.0, 0.0, 0.0]

#     count = len(unsplit_ds)
#     for i in tqdm(range(count)):

#         record = unsplit_ds[i]

#         pen = record.penman

#         res = AMRParser._compute_smatch(
#             record.target, pen, tokens, convert_a=True, convert_b=False
#         )

#         for k in range(3):
#             l[k] += res[k]
#     print(smatch.compute_f(*l))


# if __name__ == "__main__":
#     test_dataset_for_conversion()
