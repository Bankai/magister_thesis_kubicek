from collections import deque
import os
from tqdm import tqdm
import numpy as np
import pandas as pd
import editdistance
import torch
from torch.utils.data import DataLoader
from statistics import mean, stdev

# Importing the T5 modules from huggingface/transformers
from transformers import (
    T5ForConditionalGeneration,
    MT5ForConditionalGeneration,
    Adafactor,
    T5Tokenizer,
    MT5Tokenizer,
)

from dataset.amr_dataset import (
    AMRWithSimpleTargetsTokenizedDataset,
    DynamicBatchSampler,
)
from dataset.parser.amr_parser import SimpleRepresentation
import argparse


def compute_relative_distance_metrics(tokenizer, predictions, actuals):
    distances = [
        editdistance.eval(a, b) / (max([len(a), len(b)]))
        for a, b in zip(
            tokenizer(predictions)["input_ids"], tokenizer(actuals)["input_ids"]
        )
    ]
    return mean(distances), stdev(distances)


def validate(tokenizer, model, device, loader, max_len, num_beams, do_all: bool = True):
    model.eval()
    predictions = []
    actuals = []
    penmans = []
    total_batches = len(loader) if do_all else None
    with torch.no_grad():
        for data in tqdm(loader, total=total_batches):
            if not do_all and len(actuals) > 200:
                break
            y = data["labels"].to(device, dtype=torch.long)
            ids = data["input_ids"].to(device, dtype=torch.long)

            # mask = data["mask"].to(device, dtype=torch.long)
            mask = (ids != -100).to(dtype=torch.float)
            ids = torch.maximum(ids, torch.zeros_like(ids))
            y = torch.maximum(y, torch.zeros_like(y))

            generated_ids = model.generate(
                input_ids=ids,
                attention_mask=mask,
                max_length=max_len,
                num_beams=num_beams,
                early_stopping=True,
            )

            preds = [
                tokenizer.decode(
                    g, skip_special_tokens=True, clean_up_tokenization_spaces=True
                )
                for g in generated_ids
            ]
            target = [
                tokenizer.decode(
                    t, skip_special_tokens=True, clean_up_tokenization_spaces=True
                )
                for t in y
            ]
            penmans.extend(data["penman"])

            predictions.extend(preds)
            actuals.extend(target)
    return predictions, actuals, penmans


def train(
    epoch,
    model: T5ForConditionalGeneration | MT5ForConditionalGeneration,
    device,
    loader,
    optimizer,
    scheduler,
    tokenizer: T5Tokenizer | MT5Tokenizer,
    args,
):
    model.train()
    # print(tokenizer.all_special_tokens)

    match tokenizer:
        case T5Tokenizer():
            unk_token = tokenizer.unk_token_id

    # unk_token = tokenizer.unk_token_id
    assert isinstance(unk_token, int)
    Epoch = epoch + 1
    maxlen = 10000
    losses = deque(maxlen=maxlen)
    total_batches = len(loader)
    optimizer.zero_grad()

    batch_index = 0

    def apply_grad():
        nonlocal batch_index
        optimizer.step()
        optimizer.zero_grad()
        batch_index = 0

    loss_function = torch.nn.CrossEntropyLoss(
        ignore_index=-100, label_smoothing=args.label_smoothing
    )

    for batch_number, data in (
        progress_bar := tqdm(enumerate(loader, 1), total=total_batches)
    ):

        batch_data = {}
        labels = data["labels"].to(device, dtype=torch.long)
        batch_data["labels"] = labels
        ids = data["input_ids"].to(device, dtype=torch.long)

        mask = (ids != -100).to(dtype=torch.float)
        batch_data["attention_mask"] = mask
        ids = torch.maximum(ids, torch.zeros_like(ids))
        batch_data["input_ids"] = ids
        if args.masking > 0.0:
            mask_token_indices = torch.logical_and(
                torch.rand_like(ids, dtype=torch.float) < args.masking, (mask > 0)
            )
            ids[mask_token_indices] = unk_token
            batch_data["input_ids"] = ids

        outputs = model(**batch_data)

        # move labels to correct device to enable PP
        lm_logits = outputs.logits
        loss = loss_function(lm_logits.view(-1, lm_logits.size(-1)), labels.view(-1))
        # loss = outputs.loss

        loss.backward()
        batch_index += data["input_ids"].size(0)

        if batch_index >= args.apply_grad_after:
            apply_grad()

        losses.append(loss.item())

        progress_bar.set_description(
            f"{Epoch=} avg loss: {sum(losses)/len(losses):0.5f}"
        )
    if batch_index != 0:
        apply_grad()

    scheduler.step()


def collate(batch):
    data = {}
    for k in ["input_ids", "labels"]:
        data[k] = [item[k] for item in batch]
    for k in data:
        data[k] = torch.nn.utils.rnn.pad_sequence(
            data[k], batch_first=True, padding_value=-100
        )
    for k in ["penman"]:
        data[k] = [item[k] for item in batch]
    return data


def do_validation(
    tokenizer,
    model,
    device,
    val_loader,
    num_beams=2,
    max_len=1000,
    output_dir="./outputs/",
    iteration_number=5,
    do_all=True,
):

    predictions, actuals, penmans = validate(
        tokenizer,
        model,
        device,
        val_loader,
        num_beams=num_beams,
        max_len=max_len,
        do_all=do_all,
    )
    final_df = pd.DataFrame({"Generated Text": predictions, "Actual Text": actuals})
    final_df.to_csv(os.path.join(output_dir, "predictions.csv"))
    _mean, _stdev = compute_relative_distance_metrics(tokenizer, predictions, actuals)

    smatch = SimpleRepresentation.compute_smatch(
        predictions,
        penmans,
        set(tokenizer.get_added_vocab().keys()),
        convert_b=False,
        iteration_number=iteration_number,
    )
    print(f"{_mean=}+-{_stdev}, {smatch=}")


def neftune_post_forward_hook(module, input, output):
    if module.training:
        dims = torch.tensor(output.size(1) * output.size(2))
        mag_norm = module.neftune_noise_alpha / torch.sqrt(dims)
        output = output + torch.zeros_like(output).uniform_(-mag_norm, mag_norm)
    return output


def load_model(args, tokenizer):
    print(f"""[Model]: Loading {args.model}...\n""")
    if "mt5" in args.model:
        model = MT5ForConditionalGeneration
    else:
        model = T5ForConditionalGeneration
    model = model.from_pretrained(
        args.model,
        cache_dir=".cache/hugging_face",
    )
    assert isinstance(model, T5ForConditionalGeneration)
    model.resize_token_embeddings(len(tokenizer))  # type: ignore

    if args.neftune > 0.0:
        model.shared.neftune_noise_alpha = args.neftune
        hook_handle = model.shared.register_forward_hook(neftune_post_forward_hook)
    # self.neftune_hook_handle = hook_handle
    model = model.to(args.device)  # type: ignore
    return model


def T5Trainer(
    args,
):
    output_dir = args.output_dir
    torch.manual_seed(args.seed)  # pytorch random seed
    np.random.seed(args.seed)  # numpy random seed

    print(f"""[Dataset]: Loading...\n""")
    ds_split = AMRWithSimpleTargetsTokenizedDataset.get_split_dataset(args.model)
    tokenizer = ds_split.train.tokenizer

    model = load_model(args, ds_split.train.tokenizer)

    # Defining the parameters for creation of dataloaders
    training_sampler = DynamicBatchSampler(
        ds_split.train,
        max_batch_size=args.max_batch,
        work_efficiency=args.sampler_eff,
        deactivate_last=args.deactivate_last,
        max_work=args.max_work,
    )

    training_loader = DataLoader(
        ds_split.train,
        batch_sampler=training_sampler,
        collate_fn=collate,
        num_workers=0,
    )

    dev_sampler = DynamicBatchSampler(
        ds_split.dev, max_batch_size=args.max_batch, work_efficiency=args.sampler_eff
    )
    dev_loader = DataLoader(
        ds_split.dev,
        batch_sampler=dev_sampler,
        collate_fn=collate,
        num_workers=0,
    )

    test_sampler = DynamicBatchSampler(
        ds_split.test, max_batch_size=args.max_batch, work_efficiency=args.sampler_eff
    )
    test_loader = DataLoader(
        ds_split.test,
        batch_sampler=test_sampler,
        collate_fn=collate,
        num_workers=0,
    )

    # Creation of Dataloaders for testing and validation. This will be used down for training and validation stage for the model.

    # Defining the optimizer that will be used to tune the weights of the network in the training session.
    optimizer = Adafactor(
        params=model.parameters(),
        lr=args.learning_rate,
        scale_parameter=False,
        relative_step=False,
        weight_decay=args.weight_decay,
    )
    scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=args.epochs)

    # Training loop
    print(f"[Initiating Fine Tuning]...\n")

    for epoch in range(args.epochs):
        train(
            epoch=epoch,
            model=model,
            device=args.device,
            loader=training_loader,
            optimizer=optimizer,
            scheduler=scheduler,
            tokenizer=tokenizer,
            args=args,
        )

        if (epoch + 1) % args.validation == 0:
            print(f"[Saving Model]...\n")
            # Saving the model after training
            path = os.path.join(output_dir, "model_files")
            model.save_pretrained(path)
            tokenizer.save_pretrained(path)
            print("[Dev Validation]")
            do_validation(
                tokenizer,
                model,
                args.device,
                dev_loader,
                num_beams=args.beams,
                max_len=dev_sampler.max_work + 10,
                output_dir=output_dir,
            )
            print("[Training Validation]")
            do_validation(
                tokenizer,
                model,
                args.device,
                training_loader,
                num_beams=args.beams,
                max_len=training_sampler.max_work + 10,
                output_dir=output_dir,
                do_all=False,
            )

    if (epoch + 1) % args.validation != 0:
        print("[Dev Validation]")
        do_validation(
            tokenizer,
            model,
            args.device,
            dev_loader,
            num_beams=args.final_beams,
            max_len=dev_sampler.max_work + 10,
            output_dir=output_dir,
            iteration_number=10,
        )
    print("[Test Validation]")
    do_validation(
        tokenizer,
        model,
        args.device,
        test_loader,
        num_beams=args.final_beams,
        max_len=test_sampler.max_work + 10,
        output_dir=output_dir,
        iteration_number=10,
    )


def load_and_do_validation(args):
    torch.manual_seed(args.seed)  # pytorch random seed
    np.random.seed(args.seed)  # numpy random seed
    print(f"""[Dataset]: Loading...\n""")
    ds_split = AMRWithSimpleTargetsTokenizedDataset.get_split_dataset(args.model)

    model = T5ForConditionalGeneration.from_pretrained("./outputs/model_files")
    model.to(args.device)  # type: ignore
    tokenizer: T5Tokenizer = T5Tokenizer.from_pretrained("./outputs/model_files")
    dev_sampler = DynamicBatchSampler(
        ds_split.dev, max_batch_size=args.max_batch, work_efficiency=args.sampler_eff
    )
    dev_loader = DataLoader(
        ds_split.dev,
        batch_sampler=dev_sampler,
        collate_fn=collate,
        num_workers=0,
    )

    test_sampler = DynamicBatchSampler(
        ds_split.test, max_batch_size=args.max_batch, work_efficiency=args.sampler_eff
    )
    test_loader = DataLoader(
        ds_split.test,
        batch_sampler=test_sampler,
        collate_fn=collate,
        num_workers=0,
    )
    print("[Dev Validation]")
    do_validation(
        tokenizer,
        model,
        args.device,
        dev_loader,
        num_beams=args.final_beams,
        max_len=dev_sampler.max_work + 10,
        iteration_number=10,
    )
    print("[Test Validation]")
    do_validation(
        tokenizer,
        model,
        args.device,
        test_loader,
        num_beams=args.final_beams,
        max_len=test_sampler.max_work + 10,
        iteration_number=10,
    )


def main():

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("--learning_rate", type=float, default=5e-4)
    # arg_parser.add_argument("--learning_ratio", type=float, default=0.9)

    # [Regularization]
    arg_parser.add_argument("--masking", type=float, default=0.15)
    arg_parser.add_argument("--neftune", type=float, default=10.0)
    arg_parser.add_argument("--weight_decay", type=float, default=0.0)
    arg_parser.add_argument("--label_smoothing", type=float, default=0.1)

    arg_parser.add_argument("--sampler_eff", type=float, default=0.75)
    arg_parser.add_argument("--model", type=str, default="t5-base")
    arg_parser.add_argument("--output_dir", type=str, default="./outputs/")
    arg_parser.add_argument(
        "--device", type=str, default="cuda" if torch.cuda.is_available() else "cpu"
    )
    arg_parser.add_argument("--epochs", type=int, default=30)
    arg_parser.add_argument("--seed", type=int, default=42)
    arg_parser.add_argument("--run_eval_only", action="store_true")
    arg_parser.add_argument("--beams", type=int, default=2)
    arg_parser.add_argument("--final_beams", type=int, default=5)
    arg_parser.add_argument("--max_batch", type=int)
    arg_parser.add_argument("--deactivate_last", type=int, default=0)
    arg_parser.add_argument("--max_work", type=int)
    arg_parser.add_argument("--apply_grad_after", type=int, default=32)
    arg_parser.add_argument("--validation", type=int, default=10)
    args = arg_parser.parse_args()
    if args.run_eval_only:
        load_and_do_validation(args)
    else:
        T5Trainer(args)


if __name__ == "__main__":
    main()
